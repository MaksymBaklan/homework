package com.binary_studio.fleet_commander.core.ship;

import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.exceptions.InsufficientPowergridException;
import com.binary_studio.fleet_commander.core.exceptions.NotAllSubsystemsFitted;
import com.binary_studio.fleet_commander.core.ship.contract.ModularVessel;
import com.binary_studio.fleet_commander.core.subsystems.AttackSubsystemImpl;
import com.binary_studio.fleet_commander.core.subsystems.DefenciveSubsystemImpl;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class DockedShip implements ModularVessel {


	//my code
	private static DockedShip someShip = new DockedShip();

	private DefenciveSubsystem defenciveSubsystem;
	private AttackSubsystem attackSubsystem;

	private String name;
	private PositiveInteger shieldHP;
	private PositiveInteger hullHP;
	private PositiveInteger capacitor;
	private PositiveInteger capacitorRegeneration;
	private PositiveInteger pg;
	private PositiveInteger speed;
	private PositiveInteger size;

	public static DockedShip construct(String name, PositiveInteger shieldHP, PositiveInteger hullHP,
			PositiveInteger powergridOutput, PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate,
			PositiveInteger speed, PositiveInteger size) {
		// TODO: Ваш код здесь :)

		someShip.name = name;
		someShip.shieldHP = shieldHP;
		someShip.hullHP = hullHP;
		someShip.capacitor = capacitorAmount;
		someShip.capacitorRegeneration = capacitorRechargeRate;
		someShip.pg = powergridOutput;
		someShip.speed = speed;
		someShip.size = size;

		return someShip;
	}

	@Override
	public void fitAttackSubsystem(AttackSubsystem subsystem) throws InsufficientPowergridException {
		// TODO: Ваш код здесь :)
		if (subsystem == null) {
			someShip.attackSubsystem = null;
		}

		int someShipPg = someShip.pg.value();
		int subsystemPgConsumption = subsystem.getPowerGridConsumption().value();

		someShip.attackSubsystem = subsystem;

		if (someShipPg > subsystemPgConsumption) {
			someShip.pg = PositiveInteger.of(someShipPg - subsystemPgConsumption);
		} else {
			attackSubsystem = null;
			throw new InsufficientPowergridException( subsystemPgConsumption - someShipPg);
		}
	}

	@Override
	public void fitDefensiveSubsystem(DefenciveSubsystem subsystem) throws InsufficientPowergridException {
		// TODO: Ваш код здесь :)

		if (subsystem == null ) {
			someShip.defenciveSubsystem = null;
		}

		int someShipPg = someShip.pg.value();
		int subsystemPgConsumption = subsystem.getPowerGridConsumption().value();

		someShip.defenciveSubsystem = subsystem;

		if (someShipPg > subsystemPgConsumption) {
			someShip.pg = PositiveInteger.of(someShipPg - subsystemPgConsumption);
		} else {
			defenciveSubsystem = null;
			throw new InsufficientPowergridException( subsystemPgConsumption - someShipPg);
		}

	}

	public CombatReadyShip undock() throws NotAllSubsystemsFitted {
		// TODO: Ваш код здесь :)
		if (someShip.attackSubsystem == null && someShip.defenciveSubsystem == null) throw NotAllSubsystemsFitted.bothMissing();
		else if (someShip.attackSubsystem == null ) throw  NotAllSubsystemsFitted.attackMissing();
		else if(someShip.defenciveSubsystem == null) throw NotAllSubsystemsFitted.defenciveMissing();

		else return new CombatReadyShip(someShip);
	}

	//my code


	public static DockedShip getSomeShip() {
		return someShip;
	}

	public DefenciveSubsystem getDefenciveSubsystem() {
		return defenciveSubsystem;
	}

	public AttackSubsystem getAttackSubsystem() {
		return attackSubsystem;
	}

	public String getName() {
		return name;
	}

	public PositiveInteger getShieldHP() {
		return shieldHP;
	}

	public PositiveInteger getHullHP() {
		return hullHP;
	}

	public PositiveInteger getCapacitor() {
		return capacitor;
	}

	public PositiveInteger getCapacitorRegeneration() {
		return capacitorRegeneration;
	}

	public PositiveInteger getPg() {
		return pg;
	}

	public PositiveInteger getSpeed() {
		return speed;
	}

	public PositiveInteger getSize() {
		return size;
	}
}
