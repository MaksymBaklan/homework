package com.binary_studio.fleet_commander.core.subsystems;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

import java.util.Random;

public final class DefenciveSubsystemImpl implements DefenciveSubsystem {

	//my code
	private  static  DefenciveSubsystemImpl someDefencive ;

	private String name;
	private PositiveInteger impactReduction;
	private PositiveInteger shieldRegen;
	private PositiveInteger hullRegen;
	private PositiveInteger capacitorUsage;
	private PositiveInteger pgRequirement;

	public static DefenciveSubsystemImpl construct(String name, PositiveInteger powergridConsumption,
			PositiveInteger capacitorConsumption, PositiveInteger impactReductionPercent,
			PositiveInteger shieldRegeneration, PositiveInteger hullRegeneration) throws IllegalArgumentException {
		// TODO: Ваш код здесь :)

		if (name == null || name.trim().equals("")) throw new IllegalArgumentException();

		someDefencive = new DefenciveSubsystemImpl();
		someDefencive.name = name;
		someDefencive.pgRequirement = powergridConsumption;
		someDefencive.capacitorUsage = capacitorConsumption;
		someDefencive.impactReduction = impactReductionPercent;
		someDefencive.shieldRegen = shieldRegeneration;
		someDefencive.hullRegen = hullRegeneration;

		return someDefencive;
	}

	@Override
	public PositiveInteger getPowerGridConsumption() {
		// TODO: Ваш код здесь :)
		return someDefencive.pgRequirement;
	}

	@Override
	public PositiveInteger getCapacitorConsumption() {
		// TODO: Ваш код здесь :)
		return someDefencive.capacitorUsage;
	}

	@Override
	public String getName() {
		// TODO: Ваш код здесь :)
		return someDefencive.name;
	}

	@Override
	public AttackAction reduceDamage(AttackAction incomingDamage) {
		// TODO: Ваш код здесь :)
		//passive DefenciveSubsystem
		someDefencive.impactReduction = PositiveInteger.of( (someDefencive.impactReduction.value() > 95) ? 95 : someDefencive.impactReduction.value() );
		PositiveInteger reducedDamage = PositiveInteger.of((int)Math.ceil((double)incomingDamage.damage.value() - (double)incomingDamage.damage.value() * someDefencive.impactReduction.value() /100));

		return new AttackAction(reducedDamage, incomingDamage.attacker, incomingDamage.target, incomingDamage.weapon);
	}

	@Override
	public RegenerateAction regenerate() {
		// TODO: Ваш код здесь :)
		return new RegenerateAction(shieldRegen, hullRegen);
	}

}
