package com.binary_studio.fleet_commander.core.subsystems;

import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;

public final class AttackSubsystemImpl implements AttackSubsystem {


	// my code

	private static AttackSubsystemImpl someWeapon = new AttackSubsystemImpl();

	private PositiveInteger optimalSpeed;
	private PositiveInteger baseDamage;
	private PositiveInteger optimalSize;
	private PositiveInteger capacitorUsage;
	private PositiveInteger pgRequirement;
	private String name;
	// my code


	public static AttackSubsystemImpl construct(String name, PositiveInteger powergridRequirments,
			PositiveInteger capacitorConsumption, PositiveInteger optimalSpeed, PositiveInteger optimalSize,
			PositiveInteger baseDamage) throws IllegalArgumentException {
		// TODO: Ваш код здесь :)

		if (name == null || name.trim().equals("")) throw new IllegalArgumentException();

		someWeapon.name = name;
		someWeapon.pgRequirement = powergridRequirments;
		someWeapon.capacitorUsage = capacitorConsumption;
		someWeapon.optimalSpeed = optimalSpeed;
		someWeapon.optimalSize = optimalSize;
		someWeapon.baseDamage = baseDamage;

		return someWeapon;
	}

	@Override
	public PositiveInteger getPowerGridConsumption() {
		// TODO: Ваш код здесь :)
		return someWeapon.pgRequirement;
	}

	@Override
	public PositiveInteger getCapacitorConsumption() {
		// TODO: Ваш код здесь :)
		return someWeapon.capacitorUsage;
	}

	@Override
	public PositiveInteger attack(Attackable target) {
		// TODO: Ваш код здесь :)
		/*	sizeReductionModifier = when targetSize> = optimalSize -> 1
				else targetSize / optimalSize
			speedReductionModifier = when targetSpeed ​​<= optimalSpeed ​​-> 1
                else optimalSpeed ​​/ (2 * targetSpeed)
			damage = baseDamage * min (sizeReductionModifier, speedReductionModifier)
		*/

		double sizeReductionModifier = target.getSize().value() >= someWeapon.optimalSize.value() ?
				1 :
				(double)target.getSize().value() / someWeapon.optimalSize.value();

		double speedReductionModifier = target.getCurrentSpeed().value() <= someWeapon.optimalSpeed.value() ?
				1 :
				(double)someWeapon.optimalSpeed.value() / (2 * target.getCurrentSpeed().value());

		int damage = (int) Math.ceil(someWeapon.baseDamage.value() * Math.min(sizeReductionModifier, speedReductionModifier));


		return PositiveInteger.of(damage);
	}

	@Override
	public String getName() {
		// TODO: Ваш код здесь :)
		return someWeapon.name;
	}


}
