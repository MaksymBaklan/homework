package com.binary_studio.academy_coin;

import java.util.Arrays;
import java.util.stream.Stream;

public final class AcademyCoin {

    private static int result ;
	private AcademyCoin() {
	}


	public static int maxProfit(Stream<Integer> prices) {
		// TODO: Implement
        result = 0; // refreshing our static variable

        if (prices == null) {
            return result;
        }
        
        Integer[] arr = prices.toArray(Integer[]::new);

        int purchasePrice = 0;
        int sellingPrice = 0;
        boolean prepareToSell = false;

        for (Integer x: arr) {
            if (!prepareToSell) {
                if (purchasePrice == 0) {
                    purchasePrice = x;
                } else {
                    //Chech do we have the best purchase price
                    if (purchasePrice >= x) {
                        purchasePrice = x;
                    } else {
                        // if next price is lower, we begin to find best selling price
                        sellingPrice = x;
                        prepareToSell = true;
                        continue;
                    }
                }
            } else {
                //Chech do we have the best selling price
                if (prepareToSell) {
                    if (sellingPrice <= x) sellingPrice = x;
                    else {
                        //if next price is bigger, we remember the new purchse price and begin a deal
                        deal(purchasePrice, sellingPrice);
                        purchasePrice = x;
                        // after the deal we restart the search best selling price
                        sellingPrice = 0;
                        prepareToSell = false;
                    }
                }
            }
        }

        //if we have an unfinished deal, we finish it
        if (purchasePrice != 0 && sellingPrice != 0) deal(purchasePrice, sellingPrice);

        return result;
	}

	private static void deal(int purchasePrice, int y) {
	    result += y - purchasePrice;
    }


}
