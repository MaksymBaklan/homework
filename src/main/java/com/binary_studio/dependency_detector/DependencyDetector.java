package com.binary_studio.dependency_detector;

import java.util.*;

public final class DependencyDetector {

	private DependencyDetector() {
	}

	/*public static void main(String[] args) {
		List<String> librList = Arrays.asList();
		List<String[]> DepList = Arrays.asList();
		DependencyList list = new DependencyList(librList, DepList);

		System.out.println(canBuild(list));
	}*/

	public static boolean canBuild(DependencyList libraries) {
		// TODO: Implement
		if (libraries == null) {
			System.err.println("Null in arguments!\n");
			return false;
		}



		List<String> librariesList =  libraries.getLibraries();
		LinkedList<String[]> dependensiesList = new LinkedList<>(libraries.getDependencies());

		// if our list are empty
        if (librariesList.size() == 0 || dependensiesList.size() == 0) return true;

		ArrayList<String> checkList1 = new ArrayList<>();
		ArrayList<String> checkList2 = new ArrayList<>();

		//first check
		for (int i = 0, n = dependensiesList.size(); i < n; i++ ) {

			checkList1.add(dependensiesList.get(i)[0]);
			checkList2.add(dependensiesList.get(i)[1]);


			String [] tmpArr = changeDependenciesPosition(dependensiesList.get(i));
			for (int j = i + 1; j < n; j++) {
				if (dependensiesList.get(j)[0].equals(tmpArr[0]) &&  dependensiesList.get(1)[1].equals(tmpArr[1]))  {
					//System.out.println("first check");
					return false;
				}
			}
		}

		//second check

		if (checkList1.size() == librariesList.size()) {
			//System.out.println("second check");
			return false;
		}

		// third check

		String [] arrCheckList1 = checkList1.stream().sorted().toArray(String[]::new);
		String [] arrCheckList2 = checkList2.stream().sorted().toArray(String[]::new);
		int count = 0;
		for (int i = 0, n = arrCheckList1.length; i < n; i++) {
			if (count >= 2) {
				//System.out.println("third check");
				return false;
			}
			if (arrCheckList1[i].equals(arrCheckList2[i])) count++;
		}

		return true;
	}

	private static String[] changeDependenciesPosition(String[] dependencies) {
		String[] result = new String[2];

		result[0] = dependencies[1];
		result[1] = dependencies[0];

		return result;
	}

}
